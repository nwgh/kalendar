# kalendar

Implementation of a calendar of the church year in Python.

By default it uses the calendar from the [Book of Common Prayer (1662)][bcp] of the Church of England as modified by the [Revised Table of Lessons Measure 1922.][rtbl] At present using a different calendar requires modifying the source code of `kalendar.py`, but in future I hope to make it so you can load different calendars as simple modules. It will probably only ever support relatively simple  calendars — the Roman calendar, for instance, is extremely complicated and almost certainly can’t be shoe-horned into the framework in which this code thinks.

[bcp]: https://www.churchofengland.org/prayer-and-worship/worship-texts-and-resources/book-common-prayer
[rtbl]: http://www.legislation.gov.uk/ukcm/Geo5/12-13/3

This software is licenced under the EUPL. If these terms are not acceptable to you, please contact me to enquire about commerical licencing or other arrangements.

## Assumptions

- The church and civil year are both based on the Gregorian calendar, and Easter is Gregorian Easter.
- The civil year runs from 1 January to 31 December.
- The church year runs from Advent Sunday to ‘Saturday in the week of the Sunday Next before Advent’.
- The church year is divided in two parts only: a period of feasts which occur relative to fixed dates in the civil year (specifically, Advent (four Sundays before Christmas) and the time after Christmas, 25 December); and a period of feasts which occur relative to Easter Sunday. When I add support for the American prayer book and the Revised Common Lectionary, it will be possible to talk of a third period, the ‘Proper of the Church Year’ after Pentecost, relative once again to some fixed calendar dates.
- There are basically three classes of feasts:
   - Sundays and Holy Days of the Proper of Time. Every named weekday after one of these is assumed to be single and unambiguous (i.e. there will never be more than six days after them). They occur at fixed times relative either to the civil calendar, or to Easter. Most of them occur every year unfailingly, except for those at the ends of Epiphany season and at the end of the whole year, when they may be deleted.
   - Red letter days, which are all observed very year without fail on particular civil calendar dates, but if those happen to collide with some important Sundays and Holy Day of the Proper of Time, they may be transferred to another date. (In the 1662 book, these all have collects, epistles, and gospels for them, and proper lessons in the 1922 lectionary.)
   - Black letter days, some of which will lapse and not be observed in certain years if they happen to collide with dates from the Proper of Time, or with a transferred red letter day.

## Usage

### Command-Line

There’s a command line tool, `almanac.py`, to generate a calendar for a particular church year in HTML form (hopefully some day in ASCII too).

To use it:

`python almanac.py YYYY > almanac.html`

Where `YYYY` is the year in which the desired church year *ends*.

### Library

```python
import kalendar
```

Create a `kalendar.Year` object for the church year you have in mind. If instantiated without an argument, it will get the current church year (according to your computer clock). Otherwise, you can pass it a `datetime.date` object and it will give you the church year for that day, or an integer, the year in which that church year *ends*. (For instance, to see the church year which started on Advent Sunday in the year 2000, the argument should be 2001.)

`(kalendar.Year).day` is the method to get information about a particular day of the selected year. Pass it a `datetime.date` object, or nothing to get the observances for the current day.

`day` returns a `Day` object which has the following properties:

- `feast`: *either* the current red letter day, *or* the *most recent* day in the proper of time.
- `days_after`: the number of days after the day given in `feast`; if 0, it’s the day itself
- `transferred`: `True` if this red letter day was transferred from its usual day; `False` otherwise;
- `other_observances`: on red letter days which are Sundays, this set contains the name of the Sunday; on other days, it contains the name of the black letter day.

You can also iterate over a `Year` to get one `Day` object for every day in that church year.

## Todo

- Make the actual data set pluggable through a modular system. In particular, support the US 1979 calendar and the Scottish Episcopal Church’s calendars.
- Replace the namedtuples with something more sensible to avoid repeating some boilerplate when instantiating them.
- Implement rogation days, ember days, and maybe vigil/fasting eves in the `other_observances` slot.
- Hook it up to the actual 1922 Table of Lessons so it can tell you what the lessons for each day are, and ideally the collects too.
- Add (colourized) ASCII mode to `almanac.py`.

