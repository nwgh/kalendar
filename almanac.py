import sys

import kalendar

def html_almanac(year):
    yield "<table>"
    last_month = None
    
    for date, day in year:
        yield "  <tr>"
        if date.month != last_month:
            yield "    <td>%s" % date.strftime('%B')
        else:
            yield "    <td>"

        yield "    <td>%s" % date.day
        yield "    <td>%s" % ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'][date.weekday()]

        date_description = ""
        if day.days_after == 0:
            feast_name = day.feast
            if day.transferred: feast_name += ' (transferred)'
            date_description += "<b>%s</b> " % feast_name

        if day.other_observances:
            descr = ', '.join('<i>%s</i>' % obs for obs in sorted(day.other_observances))
            date_description += '(%s)' % descr

        yield "    <td>%s" % date_description

        last_month = date.month

    yield "</table>"


if __name__ == '__main__':
    year = int(sys.argv[1])
    kal = kalendar.Year(year)
    extent = '/'.join(map(str, kal.calendar_years))
    print("<!DOCTYPE html>")
    print("<meta charset=utf-8>")
    print("<title>Almanac for the Church Year %s</title>" % extent)
    print("<h1>Almanac for the Church Year %s</h1>" % extent)
    
    print('\n'.join(html_almanac(kal)))

